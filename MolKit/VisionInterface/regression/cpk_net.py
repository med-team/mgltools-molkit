## Network: net2
## file written by Vision
##

## saving node Read Molecule
##
from MolKit.VisionInterface.MolKitNodes import ReadMolecule
node0 = ReadMolecule(name='Read Molecule')
masterNet.addNode(node0,132,46)
widget = node0.inputPorts[0].widget
cfg = {}
apply( widget.configure, (), cfg)
if restoreWidgetValues:
	widget.set("cv.pdb",0)

## saving node Assign Radii
##
from MolKit.VisionInterface.MolKitNodes import AssignRadii
node1 = AssignRadii(name='Assign Radii')
masterNet.addNode(node1,186,117)
widget = node1.inputPorts[1].widget
cfg = {}
apply( widget.configure, (), cfg)
if restoreWidgetValues:
	widget.set(1,0)

## saving node CPK
##
from MolKit.VisionInterface.MolKitNodes import AtomsAsCPK
node2 = AtomsAsCPK(name='CPK')
masterNet.addNode(node2,221,249)
widget = node2.inputPorts[4].widget
cfg = {'oneTurn': 20, 'lockType': 1, 'type': 'int', 'lockValue': 0, 'lockPrecision': 0, 'increment': 1, 'lockOneTurn': 0, 'lockShowLabel': 0, 'showLabel': 1, 'precision': 2, 'lockMin': 1, 'continuous': 1, 'max': None, 'lockBIncrement': 0, 'min': 3, 'lockBMax': 0, 'lockBMin': 0, 'lockMax': 1, 'lockIncrement': 1, 'lockContinuous': 0}
apply( widget.configure, (), cfg)
if restoreWidgetValues:
	widget.set(8,0)

## saving node Select Atoms
##
from MolKit.VisionInterface.MolKitNodes import NodeSelector
node3 = NodeSelector(name='Select Atoms')
masterNet.addNode(node3,90,195)
widget = node3.inputPorts[1].widget
cfg = {}
apply( widget.configure, (), cfg)
if restoreWidgetValues:
	widget.set("",0)

## saving node Viewer
##
from DejaVu.VisionInterface.DejaVuNodes import Viewer
node4 = Viewer(name='Viewer')
masterNet.addNode(node4,376,164)

## saving connections for network net2
##
masterNet.connectNodes(node0, node1,0,0)
masterNet.connectNodes(node1, node3,0,0)
masterNet.connectNodes(node3, node2,0,0)
masterNet.connectNodes(node2, node4,0,0)
