import sys
from mglutil.regression import testplus

import test_molkit

harness = testplus.TestHarness( "testAll_MolKit_ViPEr",
                                funs = [],
                                dependents = [test_molkit.harness,
                                              ],
                                )

if __name__ == '__main__':
    testplus.chdir()
    print harness
    sys.exit( len( harness))
